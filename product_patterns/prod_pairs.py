# Nessessary libs
import pandas as pd
from datetime import date

def customers_patterns(df, id_s='id', goods='goods'):
    
    """
    The function read the df with two columns: 
        - user id's;
        - goods.
    Function returns top-100 of most popular goods pairs and saving it to the file
    """
    
    # Sorting sorting goods for each user alphabetically ascending
    # It is nessessary for looking for pairs
    df = df.sort_values([id_s, goods], ascending=[True, True]).reset_index(drop=True)
    
    # Making result dictionary, looks like {(good_1, good_2), <amount_of_pair>}
    tuples_dict = {}
    
    print(f'Unique quantity of goods: {df[goods].nunique()}')
    print('Start calculating pairs. Please wait...')
    for customer in df[id_s].unique():
        temp_df = df.query('id == @customer')
        for i in temp_df[goods]:
            for j in temp_df[goods]:
                
                # here we will provide for the absence of pairs like (i,i), (j, j) or (j, i)
                if i < j:
                    
                    # if pair (i, j) is already in dict
                    if (i, j) in tuples_dict: 
                        temp_tuple = (i, j)
                        tuples_dict[temp_tuple]+=1
                    
                    # if pair (i, j) not in dict, we'll create it
                    else:
                        temp_tuple = (i, j)
                        tuples_dict.update({temp_tuple: 1})
                        
    # making lists with good_1, good_2, number_of_pairs for creating df
    print('Creating result DataFrame...')
    data_1_list, data_2_list, number_list = [], [], []
    for item, number in tuples_dict.items():
        data_1_list.append(item[0])
        data_2_list.append(item[1])
        number_list.append(number)
    
    # making result df
    res_df = pd.DataFrame({'good_1': data_1_list, 'good_2': data_2_list, 'occurrence': number_list})\
                .sort_values('occurrence', ascending=False).reset_index(drop=True).head(100)
    
    # saving top_100 pairs in csv file
    filename = f'top_product_pairs_{str(date.today().strftime("%d.%m.%Y"))}.csv'
    print(f'Saving result table in file: {filename}')
    res_df.to_csv(filename)
    print('\nDone!')
    return res_df.head(100)