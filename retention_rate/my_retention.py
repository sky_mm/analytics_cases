import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def retention_rate(reg_data, 
                   auth_data, 
                   n_days=14, 
                   z_day=0,
                   resample=None,
                   plot='heatmap'):

    """
    --------------
    Функция, считающая retention rate
    Возвращает:
        - аггрегированный ДатаСет с информацией о посещаемости для пользователей каждой когорты (когорты определяются по дате регистрации)
        - сводная таблица по посещаемости
    --------------
    Аргументы:
        reg_data  - датафрейм с датами регистраций uid из 2х стобцов: 
                                - uid       - тип int
                                - reg_date  - тип datetime.date        
        auth_data - датафрейм с датами авторизаций uid из 2х столбцов:
                                - uid       - тип int
                                - auth_date - тип datetime.date
        n_days - число дней для построения retention
        z_day  - аргумент для построения rolling retention,
                 т.е. посещаемость дял каждой когорты в день z_day и во все дни после него
                 Если z_day = 0, считается обычный retention n-го дня
        resample - позволяет аггрегировать когорты по периодам времени:
                                - 'W'  - по неделям
                                - 'M'  - по месяцам
                                - None - по дням (без аггрегации)
        plot   - аргумент для построения визуализации:
                                - 'heatmap'  для построения тепловой карты
                                - 'lineplot' для построения линейного графика
                                - None - без визуализации  
  """ 
    
    # Обрабатываем возможные ошибки
    if z_day >= n_days:
        raise ValueError('n_days value must be more than z_day')
    if resample is not None:
        if resample not in ['M', 'W']:
            raise ValueError('resample value must be only None, "W" or "M"')
        if (resample == 'W' and n_days < 7) or (resample == 'M' and n_days < 30):
            raise ValueError('n_days is less than resampling period')
    if plot is not None:
        if plot not in ['heatmap', 'lineplot']:
            raise ValueError('plot может принимать значения только "heatmap", "lineplot" или None')
            
            
    # Находим последнюю дату в ДатаСете
    last_date = auth_data.auth_date.max()
    # Высчитываем дату, от которой будем считать retention rate
    from_date = last_date - pd.to_timedelta(n_days, 'day')

    # Отбираем только пользователей с датой регистрации после from_date
    uids_regged_from_date = reg_data.query('reg_date >= @from_date').uid
    df_from_date = auth_data[['uid', 'auth_date']].query('uid in @uids_regged_from_date')
    df_from_date = df_from_date.merge(reg_data[['uid', 'reg_date']], on='uid', how='left')   

    # Считаем какой по счету текущий день авторизации по отношению к дате регистрации
    df_from_date['days_from_reg'] = (df_from_date.auth_date - df_from_date.reg_date).dt.days
    
    # Если считаем rolling retention, то все days_from_reg после z_day, считаем как z_day
    # Т.е. все посещения после z_day будут считаться за один период
    if z_day:
        df_from_date.days_from_reg = df_from_date.days_from_reg.apply(lambda x: x if x < z_day else z_day)
        # Заголовок, понадобится для графиков
        title = f'Users rolling retention by {z_day} day from last {n_days} days'
    else:
        # Заголовок, понадобится для графиков
        title = f'Users retention by last {n_days} days'
    
    # Считаем количество пользователей в каждой когорте (сколько зарегестрировались в конкретную дату)     
    regged_by_days = df_from_date\
                            .groupby('reg_date', as_index=False)\
                            .agg({'uid' : pd.Series.nunique})\
                            .rename(columns={'uid': 'regged_uids'})
    # Считаем количество посещений в различные дни для каждой когорты пользователей
    ret_table = df_from_date\
                            .groupby(['reg_date', 'days_from_reg'], as_index=False) \
                            .agg({'uid': pd.Series.nunique}) \
                            .sort_values(['reg_date', 'days_from_reg'], ascending=[True, True]) \
                            .rename(columns={'uid': 'uids'})
    
    # Объединение когорт по периодам (W - неделя, M - месяц)
    if resample is not None:
        # Тип datetime64 необходим для дальнейшего использования функцией resample
        regged_by_days.reg_date = regged_by_days.reg_date.astype('datetime64[ns]')
        ret_table     .reg_date = ret_table     .reg_date.astype('datetime64[ns]')
        
        # Считаем количество пользователей в каждой когорте
        regged_by_days = regged_by_days\
                            .resample(rule=resample, on='reg_date')\
                            .sum()\
                            .reset_index('reg_date')
        # Считаем количество посещений в различные дни для каждой когорты пользователей
        ret_table = ret_table\
                            .groupby(['days_from_reg'])\
                            .resample(rule=resample, on='reg_date')\
                            .sum()\
                            .drop('days_from_reg', axis=1)\
                            .reset_index('days_from_reg')\
                            .sort_values(['reg_date', 'days_from_reg'])\
                            .reset_index('reg_date')
        
        # Преобразуем тип обратно в datetime.date
        ret_table     .reg_date = ret_table     .reg_date.dt.date
        regged_by_days.reg_date = regged_by_days.reg_date.dt.date
    
    # Присоединияем информацию о количестве зарегестрированных пользователей в каждую дату
    ret_table = ret_table.merge(regged_by_days, on='reg_date')
    # Считаем процент посещений от изначального размера каждой когорты
    ret_table['percentage'] = np.round(ret_table.uids / ret_table.regged_uids, 5)
    # Строим сводную таблицу
    ret_pivot = ret_table[['reg_date', 'days_from_reg', 'percentage']] \
                                            .pivot(index='reg_date', columns='days_from_reg', values='percentage')
    
        
    
    # Строим графики
    sns.set_style('whitegrid')
    x_label = 'Days from registration date'
    # Тепловая карта
    if plot == 'heatmap':
        y_label = 'Registration date'
        plt.figure(figsize=(19,12))
        ax = sns.heatmap(ret_pivot, 
                         cmap='BuGn', 
                         fmt='.0%', 
                         annot=True, 
                         vmin=0.0, 
                         vmax=ret_table.percentage.quantile(0.85))
        ax.grid(which='major',color='black', linewidth=0.5, linestyle='--')
        plt.xlabel(x_label, fontdict={'fontsize': 12})
        plt.ylabel(y_label, fontdict={'fontsize': 12})
        plt.title(title, fontdict={'fontsize': 21})
    # Линейный график
    elif plot == 'lineplot':
        y_label = 'Percentage authorised'
        plt.figure(figsize=(18,10))
        plt.ylim(0, ret_table.percentage.quantile(0.75) * 2.5)
        ax = sns.lineplot(data=ret_table, 
                          x='days_from_reg', 
                          y='percentage', 
                          hue='reg_date')
        y_tick_labels = [str(int(i * 100)) + '%' for i in ax.get_yticks()]  
        ax.set_yticklabels(y_tick_labels)
        plt.xlabel(x_label, fontdict={'fontsize': 12})
        plt.ylabel(y_label, fontdict={'fontsize': 12})
        plt.title(title, fontdict={'fontsize': 21})
    # Если plot == None, то график не строим
        
    
    # Возвращаем ДатаФрейм с агрегированный информацией и сводную таблицу
    return {'table': ret_table,
            'pivot': ret_pivot.applymap(lambda x: str(np.round(x*100, 2))+'%' if ~(np.isnan(x)) else x)}